# Part A: Graph databases (Neo4j)

You can find the questions in the question document on the student portal. Every time a question aligns with the task, you can find the reference to the question. It is best to immediately go there and answer the question. 

### A.1. Introduction to Neo4j
I usually give a short demo of Neo4j but I hope that with the following instructions, you will be able to get familiar with the basic concepts in Neo4j. It's really important that you first go through this PDF before you start with the practical. 

If any questions arise, please post them on the discussion board!

- [Neo4j Introduction](https://gitlab.com/mkutmon/msb1014-2020/raw/master/data/Neo4j-Intro.pdf)

----

### A.2. Investigating human protein-protein interaction network

Go to your own sandbox and use the following two commands to load the human PPI network from STRING.

Generation PPI network

`LOAD CSV WITH HEADERS FROM "https://docs.google.com/spreadsheets/d/11vjCmZoTPhQszS9nZzgmQLQECDKiaIbrtW4CD8DCYbU/export?format=csv&id=11vjCmZoTPhQszS9nZzgmQLQECDKiaIbrtW4CD8DCYbU&gid=741786809" AS csvLine
MERGE (protein1:Protein { id: csvLine.protein1 })
MERGE (protein2:Protein { id: csvLine.protein2 })
MERGE (protein1)-[:INTERACTS { score: toInteger(csvLine.combined_score )}]-(protein2)`

Add gene names to protein nodes

`LOAD CSV WITH HEADERS FROM "https://docs.google.com/spreadsheets/d/11vjCmZoTPhQszS9nZzgmQLQECDKiaIbrtW4CD8DCYbU/export?format=csv&id=11vjCmZoTPhQszS9nZzgmQLQECDKiaIbrtW4CD8DCYbU&gid=361761440" AS csvLine
MERGE (p:Protein { id: csvLine.protein })
ON MATCH SET p.proteinName = csvLine.geneName`

Only PPIs with a very high confidence cut-off >= 900 are included and in this part, we will investigate the network structure and use two biological questions as examples to explore the network.

Investigate the network structure by displaying a random set of 100 proteins.

`MATCH (p) RETURN p LIMIT 100`

You can click on the nodes and see the protein identifier and gene name. When clicking on the edge you will see the confidence score. 

![alt text](images/neo4j-1.png "Figure 1")

----

> Question A.2.1: Basic network statistics 

The degree distribution is one of the key differences between random and real networks. Try to understand the
following query and run in on the PPI database:

`MATCH (p:Protein)-[r]-() WITH p AS nodes, count(DISTINCT r) AS degree RETURN degree, count(nodes) AS num_nodes ORDER BY degree asc`

> Question A.2.2: Degree distribution

Next, we will focus on two biological questions that we can answer with the PPI network.

**1. Interaction partners**

Using P53 as the central protein, we will study the direct neighborhood of the protein in the network. 

> Question A.2.3: TP53 neighbors

**2. Subnetwork with key genes**

If you have a list of key genes of interest (e.g. differentially expressed genes, mutated genes, etc.), Neo4j allows you
to easily build a network connecting those genes through the shortest paths between them.

Use the following 5 genes as an input for the query: P53, BRCA1, CDKN1A, TNF, PPARG.

Understand and run the query below:
`MATCH (a),(b), p=allShortestPaths((a)-[*]-(b)) WHERE a <> b AND a.proteinName IN ["TP53","BRCA1","CDKN1A","TNF","PPARG"] AND b.proteinName IN ["TP53","BRCA1","CDKN1A","TNF","PPARG"] return p`

> Question A.2.4: Subnetwork

----

### A.3. Investigating the human disease-gene network

Make sure Assignment 2 is completely finished. Then delete all nodes and links (`MATCH (n) DETACH DELETE n`). Use the following command to load the human disease-gene association network.

`LOAD CSV WITH HEADERS FROM "https://docs.google.com/spreadsheets/u/0/d/1_OAd_6_n1Y-K9mYrKDJOIgN9nY_ntxWe2QGB88mZlV4/export?format=csv&id=1_OAd_6_n1Y-K9mYrKDJOIgN9nY_ntxWe2QGB88mZlV4&gid=0" AS csvLine
MERGE (protein:Protein {id: csvLine.protein, name: csvLine.proteinName })
MERGE (disease:Disease {id: csvLine.disease, name: csvLine.diseaseName })
CREATE (protein)-[:ASSOCIATED_WITH {confidence : toFloat(csvLine.confidence)}]->(disease)`

In this part, we will investigate the network structure and use a biological question as examples to explore the network.

Investigate the network structure by displaying a random set of 100 high-confidence disease-gene associations.

`MATCH (d:Disease)-[r]-(p:Protein) WHERE r.confidence > 1 RETURN d,r,p LIMIT 100`

You can click on the nodes and see the protein identifier and gene name. When clicking on the edge you will see
the confidence score. 

> Question A.3.1: Basic network statistics

> Question A.3.2: Panic disorder

