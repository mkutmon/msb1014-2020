# Part B: igraph R package

### B.0. Installation instructions

* Tested with R version 4.0.0
* Install igraph R package version 1.2.5 (`BiocManager::install("igraph")`) 

----

### B.1. igraph tutorial

There is a fantastic tutorial online on performing network analysis with R and iGraph. Go through the following sections in the tutorial by Katherine Ognyanova (https://kateto.net/networks-r-igraph) - [R script](http://bit.ly/netscix2016) is  available! You will need to run the other sections too but focus in particular on:

- 2. Networks in igraph
- 6. Network and node descriptives
- 7. Distances and paths

You should now be able to create random, small-world and scale-free network models (2) with the algorithms explained in the lecture. Check the degree distribution and average path lengths (as explained in 6 and 7) and discuss the findings. 
