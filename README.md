# 2020/21 - MSB1014 - Network Biology

- **Author**: [Martina Summer-Kutmon](http://maastrichtuniversity.nl/martina.kutmon)
- **Last updated**: 2020-01-09

----

# Skills session 1 - Graph theory

- **Do not spend more than 4 hours on the practical!**
- **Submit the answers and indicate how far you got.**
- **Ask questions on the discussion board when you run into problems.**

In this skills session, you will learn:
- What is Neo4j and how does it work?
- How can I work with networks in R with igraph?
- How can I visualize and analyze network sin Cytoscape?
- How to connect the tools?

----

### [Part A: Graph databases](neo4j.md) (Neo4j)

- A.1. Introduction to Neo4j
- A.2. Investigating the human PPI network
- A.3. Investigating the human disease-gene network

----

### [Part B: Network analysis in R](igraph.md) (igraph)

- B.0. Installation instructions
- B.1. Networks in igraph
- B.2. Network and node descriptives
- B.3. Distances and paths

----

### [Part C: Cytoscape](cytoscape.md) [refresh]

- C.0. Installation instructions
- C.1. Network analysis and visualization

----

### Part D: Connecting tools in R 

If there is still time, look at the following tow scripts. Using neo4r, igraph and RCy3 R packages, you can connect the three tools discussed in the practical. 

- **Script 1**: [igraph2Cytoscape.R](https://gitlab.com/mkutmon/msb1014-2020/-/raw/master/data/igraph2Cytoscape.R):<br/> 
*Les mis network analysis repeated in R using igraph and loaded into Cytoscape (script also contains part about comparing the network with a random network)*

- **Script 2**: [neo4j2Cytoscape.R](https://gitlab.com/mkutmon/msb1014-2020/-/raw/master/data/neo4j2Cytoscape.R)):<br/> 
*This script repeats the panic disorder example from Neo4j in R and visualizes the resulting network in Cytoscape. Make sure you select the correct URL and password (click on your sandbox and look at the connection details.*
