# Part C: Cytoscape

Answer the questions C.1.1. - C.1.5. in the MSB1014-skills1-questions.docx at the specific points indicated in the assignment. 

## C.0 Installation instructions

- Download and install Cytoscape 3.6.0: http://chianti.ucsd.edu/cytoscape-3.6.0/
- Start Cytsocape and install the following apps (Apps > App Manager):
  - yFiles

---- 

## C.1 Network analysis and visualization
Import the Les Misérables network in Cytoscape. The nodes in the network are characters and the links represent
co-occurrences in the play. 

- File > Import > Network > File …
- Select the "lesmis.txt" file.
- Check preview (first column is source node, second column is target node, count is edge attribute)

![alt text](images/cytoscape1.png "Figure 1")

- Click OK

Apply a layout that might be more intuitive to see the network structure:
- Layout > yFiles Organic Layout

> Question C.1.1: Basic network properties

Besides the nodes and the links in the network, the input file also contained the number of co-occurrences of two
characters. We can visualize the count as the edge width in the network visualization. In the Styles tab, create a
continuous mapping for the Width property:

![alt text](images/cytoscape2.png "Figure 2")

> Question C.1.2: Edge weight

Using the integrated NetworkAnalyzer toolbox, we can easily calculate relevant network properties to study the
structure of the network. 
- Go to Tools > NetworkAnalyzer > Network Analysis > Analyze Network…
- Choose to treat the network as undirected
- In the top right of your screen you should see a very small dialog box that you can increase:

> Question C.1.3: Node degree

NetworkAnalyzer also provides some information regarding the path lengths in the network. Based on the small
world phenomenon, we know that in real networks, we expect short average path length indicating an easily
negotiable network in which information or mass can be transported throughout the network efficiently and fast. 

> Question C.1.4: Path length

Now let's visualize the node degree and cluster coefficient in the network. 
- Click on Visualize Parameters in the NetworkAnalyzer Result panel.
- Choose the Degree as the property to be mapped to node size
- Choose the Cluster Coefficient as the property to be mapped to node color and change the gradient (use
Low values to dark colors)

Now the network visualization is adapted and we can investigate the network structure visually. 

> Question C.1.5: Clustering coefficient
